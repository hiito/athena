
Contact us if none of the following formats satisfies your study.
- FTAG1: for MC study, full information on tracks/vertices
- FTAG2: for ttbar calibration, 2-lepton filter, run on data and MC
- FTAG3: for boosted large-R jet calibration using g->bb, run on data and MC
- FTAG4: just PHYS with a 1-lepton filter

You are encouraged to find more details in https://ftag.docs.cern.ch/software/derivation.

